(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["news-news-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/news/news.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/news/news.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <ion-toolbar>\n    <ion-menu-button slot=\"start\" *ngIf=\"!config.appNavigationTabs\">\n      <ion-icon name=\"menu\"></ion-icon>\n    </ion-menu-button>\n    <ion-buttons slot=\"start\">\n      <ion-back-button icon=\"arrow-back\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>\n      {{'News'| translate }}\n    </ion-title>\n    <ion-buttons slot=\"end\" *ngIf=\"!config.appNavigationTabs\">\n      <ion-button fill=\"clear\" routerLink=\"/search\" routerDirection=\"forward\">\n        <ion-icon slot=\"icon-only\" name=\"search\"></ion-icon>\n      </ion-button>\n      <ion-button fill=\"clear\" routerLink=\"/cart\" routerDirection=\"forward\">\n        <ion-icon name=\"basket\"></ion-icon>\n        <ion-badge color=\"secondary\">{{shared.cartTotalItems()}}</ion-badge>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"news-page\">\n  <!-- *ngIf=\"post.image\" -->\n  <ion-slides pager=true paginationType=\"bullets\" class=\"animated slideIn\">\n    <ion-slide *ngFor=\"let post of featuredPosts\" (click)=\"showPostDetail(post)\">\n      <ion-spinner *ngIf=\"!post.image\"></ion-spinner>\n      <ion-img src=\"{{post.image}}\" class=\"animated fadeIn\"></ion-img>\n    </ion-slide>\n  </ion-slides>\n  <!-- top Segments  -->\n  <ion-segment [(ngModel)]=\"segments\">\n    <ion-segment-button value=\"newest\">{{'Newest' |translate}}</ion-segment-button>\n    <ion-segment-button value=\"categories\">{{ 'Categories' | translate }} </ion-segment-button>\n  </ion-segment>\n\n  <div [ngSwitch]=\"segments\">\n\n    <div *ngSwitchCase=\"'newest'\">\n      <ion-list>\n        <ion-item lines=\"full\" *ngFor=\"let post of posts\" (click)=\"showPostDetail(post)\" class=\"animated fadeIn\">\n          <ion-thumbnail slot=\"start\">\n            <ion-img src=\"{{post.image}}\"></ion-img>\n          </ion-thumbnail>\n          <ion-label>\n            <h2>{{post.title.rendered}}\n              <br>\n              <small>\n                <ion-icon name=\"clock\"></ion-icon>\n                <ion-label>\n                  {{post.date}}\n                </ion-label>\n              </small>\n            </h2>\n            <p [innerHTML]=\"post.content.rendered\"></p>\n          </ion-label>\n        </ion-item>\n        <ion-infinite-scroll #infinite (ionInfinite)=\"getPosts()\">\n          <ion-infinite-scroll-content></ion-infinite-scroll-content>\n        </ion-infinite-scroll>\n      </ion-list>\n    </div>\n\n    <div *ngSwitchCase=\"'categories'\">\n      <ion-grid class=\"ion-no-padding\" class=\"animated fadeIn\">\n        <ion-row>\n          <ion-col class=\"ion-no-padding\" size=\"6\" *ngFor=\"let cat of categories\"\n            (click)=\"openPostsPage(cat.name,cat.id)\">\n            <ion-card>\n              <ion-card-header>\n                <ion-img src=\"{{cat.image}}\"></ion-img>\n              </ion-card-header>\n              <ion-card-content>\n                <ion-card-title>\n                  {{cat.name}}\n                </ion-card-title>\n                <ion-card-subtitle>\n                  {{cat.count}} {{'Posts'|translate}}\n                </ion-card-subtitle>\n              </ion-card-content>\n            </ion-card>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/news/news.module.ts":
/*!*************************************!*\
  !*** ./src/app/news/news.module.ts ***!
  \*************************************/
/*! exports provided: NewsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsPageModule", function() { return NewsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _news_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./news.page */ "./src/app/news/news.page.ts");
/* harmony import */ var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/pipes/pipes.module */ "./src/pipes/pipes.module.ts");








const routes = [
    {
        path: '',
        component: _news_page__WEBPACK_IMPORTED_MODULE_6__["NewsPage"]
    }
];
let NewsPageModule = class NewsPageModule {
};
NewsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]
        ],
        declarations: [_news_page__WEBPACK_IMPORTED_MODULE_6__["NewsPage"]]
    })
], NewsPageModule);



/***/ }),

/***/ "./src/app/news/news.page.scss":
/*!*************************************!*\
  !*** ./src/app/news/news.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".news-page ion-segment {\n  border-bottom: 1px solid #dedede;\n}\n.news-page ion-segment ion-segment-button {\n  --indicator-color-checked: var(--ion-color-primary) !important;\n  --color-checked: var(--ion-color-primary);\n  --color: var(--ion-color-primary);\n}\n.news-page ion-list ion-item ion-thumbnail {\n  margin-bottom: auto;\n}\n.news-page ion-list ion-item ion-label {\n  margin-top: 4px;\n}\n.news-page ion-list ion-item ion-label h2 {\n  font-size: 16px;\n  font-weight: bold;\n  white-space: normal;\n}\n.news-page ion-list ion-item ion-label h2 small {\n  display: flex;\n  align-items: center;\n  font-size: 14px;\n  color: #747474;\n  font-weight: normal;\n  margin-top: 5px;\n}\n.news-page ion-list ion-item ion-label h2 small ion-label {\n  padding-left: 4px;\n}\n.news-page ion-list ion-item ion-label p p:not(:first-child) {\n  display: none;\n}\n.news-page ion-list ion-item ion-label p p:first-child {\n  white-space: normal;\n  line-height: 1.4;\n  -webkit-line-clamp: 4;\n  display: -webkit-box;\n  box-sizing: border-box;\n  -webkit-box-orient: vertical;\n}\n.news-page ion-grid ion-row ion-col ion-card {\n  border-radius: 0;\n  box-shadow: none;\n  text-align: center;\n  margin: 0;\n  box-shadow: inset 0px 0.1px lightgrey;\n}\n.news-page ion-grid ion-row ion-col ion-card ion-card-header ion-img {\n  height: 120px;\n  margin-left: auto;\n  margin-right: auto;\n}\n.news-page ion-grid ion-row ion-col ion-card ion-card-content ion-card-title {\n  font-size: 14px;\n  font-weight: bold;\n  color: #000;\n}\n.news-page ion-grid ion-row ion-col ion-card ion-card-content ion-card-subtitle {\n  font-size: 14px;\n  color: #747474;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9ncmVnZC9EZXNrdG9wL0dyZWdzIFdvcmtzcGFjZS9Xb3JrL1RyYXZlbFNvZnQvZS1zaG9wIC95ZHJhY29yZnUvc3JjL2FwcC9uZXdzL25ld3MucGFnZS5zY3NzIiwic3JjL2FwcC9uZXdzL25ld3MucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsZ0NBQUE7QUNBSjtBRENJO0VBQ0UsOERBQUE7RUFDQSx5Q0FBQTtFQUNBLGlDQUFBO0FDQ047QURLTTtFQUNFLG1CQUFBO0FDSFI7QURLTTtFQUNFLGVBQUE7QUNIUjtBRElRO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUNGVjtBREdVO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUNEWjtBREVZO0VBQ0UsaUJBQUE7QUNBZDtBREtVO0VBQ0UsYUFBQTtBQ0haO0FES1U7RUFDRSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxvQkFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7QUNIWjtBRGFRO0VBUUUsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLHFDQUFBO0FDbEJWO0FEUVk7RUFDRSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQ05kO0FEZVk7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0FDYmQ7QURlWTtFQUNFLGVBQUE7RUFDQSxjQUFBO0FDYmQiLCJmaWxlIjoic3JjL2FwcC9uZXdzL25ld3MucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm5ld3MtcGFnZSB7XG4gIGlvbi1zZWdtZW50IHtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RlZGVkZTtcbiAgICBpb24tc2VnbWVudC1idXR0b24ge1xuICAgICAgLS1pbmRpY2F0b3ItY29sb3ItY2hlY2tlZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpICFpbXBvcnRhbnQ7XG4gICAgICAtLWNvbG9yLWNoZWNrZWQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICAgIC0tY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbiAgICB9XG4gIH1cblxuICBpb24tbGlzdCB7XG4gICAgaW9uLWl0ZW0ge1xuICAgICAgaW9uLXRodW1ibmFpbCB7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IGF1dG87XG4gICAgICB9XG4gICAgICBpb24tbGFiZWwge1xuICAgICAgICBtYXJnaW4tdG9wOiA0cHg7XG4gICAgICAgIGgyIHtcbiAgICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbiAgICAgICAgICBzbWFsbCB7XG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgICAgIGNvbG9yOiAjNzQ3NDc0O1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDVweDtcbiAgICAgICAgICAgIGlvbi1sYWJlbCB7XG4gICAgICAgICAgICAgIHBhZGRpbmctbGVmdDogNHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBwIHtcbiAgICAgICAgICBwOm5vdCg6Zmlyc3QtY2hpbGQpIHtcbiAgICAgICAgICAgIGRpc3BsYXk6IG5vbmU7XG4gICAgICAgICAgfVxuICAgICAgICAgIHA6Zmlyc3QtY2hpbGQge1xuICAgICAgICAgICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbiAgICAgICAgICAgIGxpbmUtaGVpZ2h0OiAxLjQ7XG4gICAgICAgICAgICAtd2Via2l0LWxpbmUtY2xhbXA6IDQ7XG4gICAgICAgICAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcbiAgICAgICAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgICAgICAgICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIGlvbi1ncmlkIHtcbiAgICBpb24tcm93IHtcbiAgICAgIGlvbi1jb2wge1xuICAgICAgICBpb24tY2FyZCB7XG4gICAgICAgICAgaW9uLWNhcmQtaGVhZGVyIHtcbiAgICAgICAgICAgIGlvbi1pbWcge1xuICAgICAgICAgICAgICBoZWlnaHQ6IDEyMHB4O1xuICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xuICAgICAgICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgICBib3gtc2hhZG93OiBpbnNldCAwcHggMC4xcHggbGlnaHRncmV5O1xuICAgICAgICAgIGlvbi1jYXJkLWNvbnRlbnQge1xuICAgICAgICAgICAgaW9uLWNhcmQtdGl0bGUge1xuICAgICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICAgICAgICBjb2xvcjogIzAwMDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlvbi1jYXJkLXN1YnRpdGxlIHtcbiAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICBjb2xvcjogIzc0NzQ3NDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiIsIi5uZXdzLXBhZ2UgaW9uLXNlZ21lbnQge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2RlZGVkZTtcbn1cbi5uZXdzLXBhZ2UgaW9uLXNlZ21lbnQgaW9uLXNlZ21lbnQtYnV0dG9uIHtcbiAgLS1pbmRpY2F0b3ItY29sb3ItY2hlY2tlZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpICFpbXBvcnRhbnQ7XG4gIC0tY29sb3ItY2hlY2tlZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICAtLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG59XG4ubmV3cy1wYWdlIGlvbi1saXN0IGlvbi1pdGVtIGlvbi10aHVtYm5haWwge1xuICBtYXJnaW4tYm90dG9tOiBhdXRvO1xufVxuLm5ld3MtcGFnZSBpb24tbGlzdCBpb24taXRlbSBpb24tbGFiZWwge1xuICBtYXJnaW4tdG9wOiA0cHg7XG59XG4ubmV3cy1wYWdlIGlvbi1saXN0IGlvbi1pdGVtIGlvbi1sYWJlbCBoMiB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHdoaXRlLXNwYWNlOiBub3JtYWw7XG59XG4ubmV3cy1wYWdlIGlvbi1saXN0IGlvbi1pdGVtIGlvbi1sYWJlbCBoMiBzbWFsbCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6ICM3NDc0NzQ7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIG1hcmdpbi10b3A6IDVweDtcbn1cbi5uZXdzLXBhZ2UgaW9uLWxpc3QgaW9uLWl0ZW0gaW9uLWxhYmVsIGgyIHNtYWxsIGlvbi1sYWJlbCB7XG4gIHBhZGRpbmctbGVmdDogNHB4O1xufVxuLm5ld3MtcGFnZSBpb24tbGlzdCBpb24taXRlbSBpb24tbGFiZWwgcCBwOm5vdCg6Zmlyc3QtY2hpbGQpIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cbi5uZXdzLXBhZ2UgaW9uLWxpc3QgaW9uLWl0ZW0gaW9uLWxhYmVsIHAgcDpmaXJzdC1jaGlsZCB7XG4gIHdoaXRlLXNwYWNlOiBub3JtYWw7XG4gIGxpbmUtaGVpZ2h0OiAxLjQ7XG4gIC13ZWJraXQtbGluZS1jbGFtcDogNDtcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XG59XG4ubmV3cy1wYWdlIGlvbi1ncmlkIGlvbi1yb3cgaW9uLWNvbCBpb24tY2FyZCB7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luOiAwO1xuICBib3gtc2hhZG93OiBpbnNldCAwcHggMC4xcHggbGlnaHRncmV5O1xufVxuLm5ld3MtcGFnZSBpb24tZ3JpZCBpb24tcm93IGlvbi1jb2wgaW9uLWNhcmQgaW9uLWNhcmQtaGVhZGVyIGlvbi1pbWcge1xuICBoZWlnaHQ6IDEyMHB4O1xuICBtYXJnaW4tbGVmdDogYXV0bztcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xufVxuLm5ld3MtcGFnZSBpb24tZ3JpZCBpb24tcm93IGlvbi1jb2wgaW9uLWNhcmQgaW9uLWNhcmQtY29udGVudCBpb24tY2FyZC10aXRsZSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjMDAwO1xufVxuLm5ld3MtcGFnZSBpb24tZ3JpZCBpb24tcm93IGlvbi1jb2wgaW9uLWNhcmQgaW9uLWNhcmQtY29udGVudCBpb24tY2FyZC1zdWJ0aXRsZSB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgY29sb3I6ICM3NDc0NzQ7XG59Il19 */");

/***/ }),

/***/ "./src/app/news/news.page.ts":
/*!***********************************!*\
  !*** ./src/app/news/news.page.ts ***!
  \***********************************/
/*! exports provided: NewsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewsPage", function() { return NewsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/providers/config/config.service */ "./src/providers/config/config.service.ts");
/* harmony import */ var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/providers/loading/loading.service */ "./src/providers/loading/loading.service.ts");
/* harmony import */ var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/providers/shared-data/shared-data.service */ "./src/providers/shared-data/shared-data.service.ts");







let NewsPage = class NewsPage {
    constructor(navCtrl, http, config, loading, shared) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.config = config;
        this.loading = loading;
        this.shared = shared;
        this.featuredPosts = new Array;
        this.segments = 'newest';
        //WordPress intergation
        this.categories = new Array;
        //page varible
        this.page = 1;
        //WordPress intergation
        this.posts = new Array;
        //page varible
        this.page2 = 1;
        this.loadingServerDataPosts = true;
        //========================================= tab newest categories ===============================================================================
        this.getCategories = function () {
            var data = {};
            data.language_id = this.config.langId;
            data.page_number = this.page2;
            this.config.getWithUrl(this.config.url + '/wp-json/wp/v2/categories/?page=' + this.page2 + "&" + this.config.productsArguments).then((data) => {
                if (this.page2 == 1) {
                    this.categories = [];
                }
                this.page2++;
                data.forEach((value, index) => {
                    this.categories.push(Object.assign(value, { image: '' }));
                });
                // console.log(data.data.length);
                if (data.length == 0) { // if we get less than 10 products then infinite scroll will de disabled
                    //this.shared.toast("All Categories Loaded!");
                    this.getRandomImage();
                }
                else
                    this.getCategories();
            }, function (response) {
                // console.log("Error while loading categories from the server");
                // console.log(response);
            });
        };
        var dat = {};
        dat.language_id = this.config.langId;
        dat.is_feature = 1;
        this.config.getWithUrl(this.config.url + '/wp-json/wp/v2/posts/?sticky=true&page=' + this.page2 + "&" + this.config.productsArguments).then((data) => {
            data.forEach((value, index) => {
                this.getImagePost(value);
            });
            this.featuredPosts = data;
        });
        this.getPosts();
    }
    getImagePost(post) {
        post.image = "assets/placeholder.png";
        if (post._links["wp:featuredmedia"])
            this.config.getWithUrl(post._links["wp:featuredmedia"][0].href).then((data) => {
                post.image = data.source_url;
            });
    }
    //============================================================================================  
    //getting list of posts
    getPosts() {
        if (this.page == 1) {
            this.loading.show();
            this.loadingServerDataPosts = false;
        }
        this.config.getWithUrl(this.config.url + '/wp-json/wp/v2/posts/?page=' + this.page + "&" + this.config.productsArguments).then((data) => {
            this.infinite.complete(); //stopping infinite scroll loader
            if (this.page == 1) {
                this.posts = [];
                this.infinite.disabled = false;
                this.loading.hide();
                this.getCategories();
            }
            this.page++;
            data.forEach((value, index) => {
                this.getImagePost(value);
                this.posts.push(value);
            });
            this.posts.concat(data);
            if (data.length < 9) { // if we get less than 10 products then infinite scroll will de disabled
                this.infinite.disabled = true; //disabling infinite scroll
                if (this.posts.length != 0) {
                    //this.shared.toast("All Posts Loaded!");
                }
            }
            this.loadingServerDataPosts = true;
        }, err => {
            this.infinite.disabled = true;
            // console.log("Error while loading posts from the server");
            // console.log(response);
        });
    }
    ;
    //============================================================================================  
    //getting list of sub categories from the server
    showPostDetail(post) {
        this.shared.singlePostData = post;
        if (this.config.appNavigationTabs)
            this.navCtrl.navigateForward(this.config.currentRoute + "/news-detail");
        else
            this.navCtrl.navigateForward("/news-detail");
    }
    ;
    openPostsPage(name, id) {
        if (this.config.appNavigationTabs)
            this.navCtrl.navigateForward(this.config.currentRoute + "/news-list/" + id + "/" + name);
        else
            this.navCtrl.navigateForward("/news-list/" + id + "/" + name);
    }
    ionViewWillEnter() {
        if (this.config.admob == 1)
            this.shared.showAd();
    }
    // <!-- 2.0 updates -->
    getRandomImage() {
        try {
            this.categories.forEach((value, index) => {
                value.image = "assets/placeholder.png";
                let rand = this.getRandomPost();
                if (rand._links["wp:featuredmedia"])
                    this.config.getWithUrl(rand._links["wp:featuredmedia"][0].href).then((data) => {
                        value.image = data.source_url;
                        console.log(value.image);
                    });
            });
        }
        catch (error) {
        }
    }
    getRandomPost() {
        let rand = this.posts[Math.floor(Math.random() * this.posts.length)];
        if (rand.sticky == false)
            return rand;
        else
            this.getRandomPost();
    }
    ngOnInit() {
    }
};
NewsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
    { type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"] },
    { type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_5__["LoadingService"] },
    { type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_6__["SharedDataService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"], { static: false }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonInfiniteScroll"])
], NewsPage.prototype, "infinite", void 0);
NewsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-news',
        encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./news.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/news/news.page.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./news.page.scss */ "./src/app/news/news.page.scss")).default]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"],
        src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_4__["ConfigService"],
        src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_5__["LoadingService"],
        src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_6__["SharedDataService"]])
], NewsPage);



/***/ })

}]);
//# sourceMappingURL=news-news-module-es2015.js.map