function _createForOfIteratorHelper(o) { if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (o = _unsupportedIterableToArray(o))) { var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var it, normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(n); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["reviews-reviews-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/reviews/reviews.page.html":
  /*!*********************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/reviews/reviews.page.html ***!
    \*********************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppReviewsReviewsPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button icon=\"arrow-back\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>{{'Reviews'|translate}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"review-page\">\n  <ion-grid class=\"rating-gird\" *ngIf=\"r5!=null\">\n    <ion-row>\n      <ion-col>\n        <h4>{{'Ratings & Reviews'|translate}}</h4>\n      </ion-col>\n    </ion-row>\n\n    <ion-row class=\"ion-align-items-center\">\n      <ion-col size=\"5\">\n        <h1>{{average| number:'1.2-2'}}\n          <ion-icon name=\"star\"></ion-icon>\n        </h1>\n        <h3>{{reviews.length}} {{'rating'|translate}}</h3>\n      </ion-col>\n      <ion-col size=\"7\">\n        <ul>\n          <li>5\n            <ion-icon name=\"star\"></ion-icon>\n            <span class=\"block animate\" [style.width]=\"r5+'%'\" style=\"background-color:green;\"></span>\n          </li>\n          <li>4\n            <ion-icon name=\"star\"></ion-icon>\n            <span class=\"block animate\" [style.width]=\"r4+'%'\" style=\"background-color:lightgreen;\"></span>\n          </li>\n          <li>3\n            <ion-icon name=\"star\"></ion-icon>\n            <span class=\"block animate\" [style.width]=\"r3+'%'\" style=\"background-color:yellow;\"></span>\n          </li>\n          <li>2\n            <ion-icon name=\"star\"></ion-icon>\n            <span class=\"block animate\" [style.width]=\"r2+'%'\" style=\"background-color:orange;\"></span>\n          </li>\n          <li>1\n            <ion-icon name=\"star\"></ion-icon>\n            <span class=\"block animate\" [style.width]=\"r1+'%'\" style=\"background-color:dar=darkorange;\"></span>\n          </li>\n        </ul>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <ion-button expand=\"full\" color=\"danger\" (click)=\"openReviewsPage()\">{{'Rate and write a review'|translate}}\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-list class=\"rating-users\">\n    <ion-item *ngFor=\"let r of reviews\">\n      <ion-avatar item-start>\n        <img src=\"assets/avatar.png\">\n      </ion-avatar>\n      <h2>{{r.name}}</h2>\n      <ul class=\"product-rating\">\n        <li>\n          <div class=\"stars-outer\">\n            <div class=\"stars-inner\" [style.width]=\"(r.rating*20)+'%'\"></div>\n          </div>\n          <h6>{{r.date_created|date:'dd-MM-yyyy'}}</h6>\n        </li>\n      </ul>\n      <p [innerHTML]=\"r.review\"></p>\n    </ion-item>\n  </ion-list>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/reviews/reviews.module.ts":
  /*!*******************************************!*\
    !*** ./src/app/reviews/reviews.module.ts ***!
    \*******************************************/

  /*! exports provided: ReviewsPageModule */

  /***/
  function srcAppReviewsReviewsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ReviewsPageModule", function () {
      return ReviewsPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _reviews_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./reviews.page */
    "./src/app/reviews/reviews.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _reviews_page__WEBPACK_IMPORTED_MODULE_6__["ReviewsPage"]
    }];

    var ReviewsPageModule = function ReviewsPageModule() {
      _classCallCheck(this, ReviewsPageModule);
    };

    ReviewsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]],
      declarations: [_reviews_page__WEBPACK_IMPORTED_MODULE_6__["ReviewsPage"]]
    })], ReviewsPageModule);
    /***/
  },

  /***/
  "./src/app/reviews/reviews.page.scss":
  /*!*******************************************!*\
    !*** ./src/app/reviews/reviews.page.scss ***!
    \*******************************************/

  /*! exports provided: default */

  /***/
  function srcAppReviewsReviewsPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "@charset \"UTF-8\";\n.review-page {\n  background-color: white;\n}\n.review-page .rating-gird h4 {\n  margin-top: 0;\n  margin-bottom: 0;\n}\n.review-page .rating-gird h1 {\n  font-size: 30px;\n  margin-top: 0;\n  text-align: center;\n}\n.review-page .rating-gird h3 {\n  color: var(--ion-color-secondary);\n  margin-bottom: 0;\n  text-align: center;\n}\n.review-page .rating-gird ul {\n  padding-left: 15px;\n  margin-bottom: 0;\n  border-left: 1px solid #ccc;\n}\n.review-page .rating-gird ul li {\n  display: flex;\n  align-items: center;\n  font-size: 16px;\n  list-style: none;\n  position: relative;\n}\n.review-page .rating-gird ul li .icon {\n  margin-left: 5px;\n}\n.review-page .rating-gird ul li .block {\n  height: 11px;\n  display: inline-block;\n  margin-left: 7px;\n  position: relative;\n  z-index: 2;\n}\n.review-page .rating-gird ul li::after {\n  content: \"\";\n  background-color: #f4f4f4;\n  height: 11px;\n  width: calc(100% - 36px);\n  position: absolute;\n  left: 36px;\n  z-index: 1;\n  top: 5px;\n}\n.review-page .rating-gird .button {\n  min-height: 45px;\n}\n.review-page .rating-users .item {\n  background-color: transparent;\n  align-items: flex-start;\n}\n.review-page .rating-users .item ion-avatar {\n  margin-top: 10px;\n}\n.review-page .rating-users .item h2 {\n  font-weight: bold;\n}\n.review-page .rating-users .item .product-rating {\n  margin-top: 0;\n  margin-bottom: 0;\n  padding-left: 0;\n}\n.review-page .rating-users .item .product-rating li {\n  display: flex;\n  align-items: center;\n}\n.review-page .rating-users .item .product-rating li h6 {\n  margin-top: 5px;\n  margin-left: 10px;\n}\n.review-page .rating-users .item p p {\n  margin-top: 2px;\n  white-space: normal;\n}\n.review-page .product-rating {\n  display: flex;\n  align-items: center;\n  justify-content: flex-start;\n}\n.review-page .product-rating .stars-outer {\n  display: inline-block;\n  position: relative;\n  font-size: 25px;\n}\n.review-page .product-rating .stars-outer::before {\n  content: \"☆☆☆☆☆\";\n  color: #ccc;\n}\n.review-page .product-rating .stars-outer .stars-inner {\n  font-size: 25px;\n  position: absolute;\n  top: 0;\n  left: 0;\n  white-space: nowrap;\n  overflow: hidden;\n}\n.review-page .product-rating .stars-outer .stars-inner::before {\n  content: \"★★★★★\";\n  color: #f8ce0b;\n}\n.review-page .product-rating h6 {\n  color: cornflowerblue;\n  margin-left: 15px;\n}\n.review-page .animate {\n  -webkit-animation: fadeInRight 0.75s;\n          animation: fadeInRight 0.75s;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcmV2aWV3cy9yZXZpZXdzLnBhZ2Uuc2NzcyIsIi9Vc2Vycy9ncmVnZC9EZXNrdG9wL0dyZWdzIFdvcmtzcGFjZS9Xb3JrL1RyYXZlbFNvZnQvZS1zaG9wIC95ZHJhY29yZnUvc3JjL2FwcC9yZXZpZXdzL3Jldmlld3MucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdCQUFnQjtBQ0FoQjtFQUNFLHVCQUFBO0FERUY7QUNBSTtFQUVFLGFBQUE7RUFDQSxnQkFBQTtBRENOO0FDRUk7RUFDRSxlQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0FEQU47QUNLSTtFQUVFLGlDQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBREpOO0FDTUk7RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsMkJBQUE7QURKTjtBQ0tNO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QURIUjtBQ0lRO0VBQ0UsZ0JBQUE7QURGVjtBQ0lRO0VBQ0UsWUFBQTtFQUNBLHFCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7QURGVjtBQ0lRO0VBQ0UsV0FBQTtFQUNBLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLHdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsVUFBQTtFQUNBLFFBQUE7QURGVjtBQ01JO0VBQ0UsZ0JBQUE7QURKTjtBQ1FJO0VBQ0UsNkJBQUE7RUFDQSx1QkFBQTtBRE5OO0FDUU07RUFDRSxnQkFBQTtBRE5SO0FDUU07RUFDRSxpQkFBQTtBRE5SO0FDUU07RUFDRSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FETlI7QUNPUTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtBRExWO0FDT1U7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QURMWjtBQ1VRO0VBQ0UsZUFBQTtFQUNBLG1CQUFBO0FEUlY7QUNjRTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDJCQUFBO0FEWko7QUNjSTtFQUNFLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0FEWk47QUNhTTtFQUNFLGdCQUFBO0VBQ0EsV0FBQTtBRFhSO0FDYU07RUFDRSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QURYUjtBQ2FRO0VBQ0UsZ0JBQUE7RUFDQSxjQUFBO0FEWFY7QUNlSTtFQUNFLHFCQUFBO0VBQ0EsaUJBQUE7QURiTjtBQ2lCRTtFQUNFLG9DQUFBO1VBQUEsNEJBQUE7QURmSiIsImZpbGUiOiJzcmMvYXBwL3Jldmlld3MvcmV2aWV3cy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAY2hhcnNldCBcIlVURi04XCI7XG4ucmV2aWV3LXBhZ2Uge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cbi5yZXZpZXctcGFnZSAucmF0aW5nLWdpcmQgaDQge1xuICBtYXJnaW4tdG9wOiAwO1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuLnJldmlldy1wYWdlIC5yYXRpbmctZ2lyZCBoMSB7XG4gIGZvbnQtc2l6ZTogMzBweDtcbiAgbWFyZ2luLXRvcDogMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLnJldmlldy1wYWdlIC5yYXRpbmctZ2lyZCBoMyB7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3Itc2Vjb25kYXJ5KTtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLnJldmlldy1wYWdlIC5yYXRpbmctZ2lyZCB1bCB7XG4gIHBhZGRpbmctbGVmdDogMTVweDtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbiAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCAjY2NjO1xufVxuLnJldmlldy1wYWdlIC5yYXRpbmctZ2lyZCB1bCBsaSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbGlzdC1zdHlsZTogbm9uZTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLnJldmlldy1wYWdlIC5yYXRpbmctZ2lyZCB1bCBsaSAuaWNvbiB7XG4gIG1hcmdpbi1sZWZ0OiA1cHg7XG59XG4ucmV2aWV3LXBhZ2UgLnJhdGluZy1naXJkIHVsIGxpIC5ibG9jayB7XG4gIGhlaWdodDogMTFweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBtYXJnaW4tbGVmdDogN3B4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHotaW5kZXg6IDI7XG59XG4ucmV2aWV3LXBhZ2UgLnJhdGluZy1naXJkIHVsIGxpOjphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmNGY0ZjQ7XG4gIGhlaWdodDogMTFweDtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDM2cHgpO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDM2cHg7XG4gIHotaW5kZXg6IDE7XG4gIHRvcDogNXB4O1xufVxuLnJldmlldy1wYWdlIC5yYXRpbmctZ2lyZCAuYnV0dG9uIHtcbiAgbWluLWhlaWdodDogNDVweDtcbn1cbi5yZXZpZXctcGFnZSAucmF0aW5nLXVzZXJzIC5pdGVtIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xufVxuLnJldmlldy1wYWdlIC5yYXRpbmctdXNlcnMgLml0ZW0gaW9uLWF2YXRhciB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4ucmV2aWV3LXBhZ2UgLnJhdGluZy11c2VycyAuaXRlbSBoMiB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuLnJldmlldy1wYWdlIC5yYXRpbmctdXNlcnMgLml0ZW0gLnByb2R1Y3QtcmF0aW5nIHtcbiAgbWFyZ2luLXRvcDogMDtcbiAgbWFyZ2luLWJvdHRvbTogMDtcbiAgcGFkZGluZy1sZWZ0OiAwO1xufVxuLnJldmlldy1wYWdlIC5yYXRpbmctdXNlcnMgLml0ZW0gLnByb2R1Y3QtcmF0aW5nIGxpIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5yZXZpZXctcGFnZSAucmF0aW5nLXVzZXJzIC5pdGVtIC5wcm9kdWN0LXJhdGluZyBsaSBoNiB7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG59XG4ucmV2aWV3LXBhZ2UgLnJhdGluZy11c2VycyAuaXRlbSBwIHAge1xuICBtYXJnaW4tdG9wOiAycHg7XG4gIHdoaXRlLXNwYWNlOiBub3JtYWw7XG59XG4ucmV2aWV3LXBhZ2UgLnByb2R1Y3QtcmF0aW5nIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xufVxuLnJldmlldy1wYWdlIC5wcm9kdWN0LXJhdGluZyAuc3RhcnMtb3V0ZXIge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZm9udC1zaXplOiAyNXB4O1xufVxuLnJldmlldy1wYWdlIC5wcm9kdWN0LXJhdGluZyAuc3RhcnMtb3V0ZXI6OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwi4piG4piG4piG4piG4piGXCI7XG4gIGNvbG9yOiAjY2NjO1xufVxuLnJldmlldy1wYWdlIC5wcm9kdWN0LXJhdGluZyAuc3RhcnMtb3V0ZXIgLnN0YXJzLWlubmVyIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbi5yZXZpZXctcGFnZSAucHJvZHVjdC1yYXRpbmcgLnN0YXJzLW91dGVyIC5zdGFycy1pbm5lcjo6YmVmb3JlIHtcbiAgY29udGVudDogXCLimIXimIXimIXimIXimIVcIjtcbiAgY29sb3I6ICNmOGNlMGI7XG59XG4ucmV2aWV3LXBhZ2UgLnByb2R1Y3QtcmF0aW5nIGg2IHtcbiAgY29sb3I6IGNvcm5mbG93ZXJibHVlO1xuICBtYXJnaW4tbGVmdDogMTVweDtcbn1cbi5yZXZpZXctcGFnZSAuYW5pbWF0ZSB7XG4gIGFuaW1hdGlvbjogZmFkZUluUmlnaHQgMC43NXM7XG59IiwiLnJldmlldy1wYWdlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIC5yYXRpbmctZ2lyZCB7XG4gICAgaDQge1xuICAgICAgLy9mb250LXNpemU6ICRoMi1mb250LXNpemU7XG4gICAgICBtYXJnaW4tdG9wOiAwO1xuICAgICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICB9XG5cbiAgICBoMSB7XG4gICAgICBmb250LXNpemU6IDMwcHg7XG4gICAgICBtYXJnaW4tdG9wOiAwO1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG4gICAgICAuaWNvbiB7XG4gICAgICB9XG4gICAgfVxuICAgIGgzIHtcbiAgICAgIC8vZm9udC1zaXplOiAkaDItZm9udC1zaXplO1xuICAgICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1zZWNvbmRhcnkpO1xuICAgICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG4gICAgdWwge1xuICAgICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xuICAgICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgI2NjYztcbiAgICAgIGxpIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICBsaXN0LXN0eWxlOiBub25lO1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIC5pY29uIHtcbiAgICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xuICAgICAgICB9XG4gICAgICAgIC5ibG9jayB7XG4gICAgICAgICAgaGVpZ2h0OiAxMXB4O1xuICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgICBtYXJnaW4tbGVmdDogN3B4O1xuICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgICB6LWluZGV4OiAyO1xuICAgICAgICB9XG4gICAgICAgICY6OmFmdGVyIHtcbiAgICAgICAgICBjb250ZW50OiBcIlwiO1xuICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmNGY0ZjQ7XG4gICAgICAgICAgaGVpZ2h0OiAxMXB4O1xuICAgICAgICAgIHdpZHRoOiBjYWxjKDEwMCUgLSAzNnB4KTtcbiAgICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgICAgbGVmdDogMzZweDtcbiAgICAgICAgICB6LWluZGV4OiAxO1xuICAgICAgICAgIHRvcDogNXB4O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIC5idXR0b24ge1xuICAgICAgbWluLWhlaWdodDogNDVweDtcbiAgICB9XG4gIH1cbiAgLnJhdGluZy11c2VycyB7XG4gICAgLml0ZW0ge1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcblxuICAgICAgaW9uLWF2YXRhciB7XG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgICB9XG4gICAgICBoMiB7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgfVxuICAgICAgLnByb2R1Y3QtcmF0aW5nIHtcbiAgICAgICAgbWFyZ2luLXRvcDogMDtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAwO1xuICAgICAgICBsaSB7XG4gICAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuXG4gICAgICAgICAgaDYge1xuICAgICAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xuICAgICAgICAgICAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgICBwIHtcbiAgICAgICAgcCB7XG4gICAgICAgICAgbWFyZ2luLXRvcDogMnB4O1xuICAgICAgICAgIHdoaXRlLXNwYWNlOiBub3JtYWw7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAucHJvZHVjdC1yYXRpbmcge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XG5cbiAgICAuc3RhcnMtb3V0ZXIge1xuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgZm9udC1zaXplOiAyNXB4O1xuICAgICAgJjo6YmVmb3JlIHtcbiAgICAgICAgY29udGVudDogXCJcXDI2MDZcXDI2MDZcXDI2MDZcXDI2MDZcXDI2MDZcIjtcbiAgICAgICAgY29sb3I6ICNjY2M7XG4gICAgICB9XG4gICAgICAuc3RhcnMtaW5uZXIge1xuICAgICAgICBmb250LXNpemU6IDI1cHg7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgdG9wOiAwO1xuICAgICAgICBsZWZ0OiAwO1xuICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgICAgICBvdmVyZmxvdzogaGlkZGVuO1xuXG4gICAgICAgICY6OmJlZm9yZSB7XG4gICAgICAgICAgY29udGVudDogXCJcXDI2MDVcXDI2MDVcXDI2MDVcXDI2MDVcXDI2MDVcIjtcbiAgICAgICAgICBjb2xvcjogI2Y4Y2UwYjtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgICBoNiB7XG4gICAgICBjb2xvcjogY29ybmZsb3dlcmJsdWU7XG4gICAgICBtYXJnaW4tbGVmdDogMTVweDtcbiAgICB9XG4gIH1cblxuICAuYW5pbWF0ZSB7XG4gICAgYW5pbWF0aW9uOiBmYWRlSW5SaWdodCAwLjc1cztcbiAgfVxufVxuIl19 */";
    /***/
  },

  /***/
  "./src/app/reviews/reviews.page.ts":
  /*!*****************************************!*\
    !*** ./src/app/reviews/reviews.page.ts ***!
    \*****************************************/

  /*! exports provided: ReviewsPage */

  /***/
  function srcAppReviewsReviewsPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ReviewsPage", function () {
      return ReviewsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _modals_login_login_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../modals/login/login.page */
    "./src/app/modals/login/login.page.ts");

    var ReviewsPage = /*#__PURE__*/function () {
      function ReviewsPage(navCtrl, activatedRoute, applicationRef, config, modalCtrl, loading, shared) {
        _classCallCheck(this, ReviewsPage);

        this.navCtrl = navCtrl;
        this.activatedRoute = activatedRoute;
        this.applicationRef = applicationRef;
        this.config = config;
        this.modalCtrl = modalCtrl;
        this.loading = loading;
        this.shared = shared;
        this.reviews = [];
        this.r1 = null;
        this.r2 = null;
        this.r3 = null;
        this.r4 = null;
        this.r5 = null;
        this.id = this.activatedRoute.snapshot.paramMap.get('id');
        this.getProductReviews();
      } //===============================================================================================================================
      // <!-- 2.0 updates -->


      _createClass(ReviewsPage, [{
        key: "getProductReviews",
        value: function getProductReviews() {
          var _this = this;

          this.loading.show();
          this.config.getWoo("products/reviews/?" + this.config.productsArguments + "&product=" + [this.id]).then(function (data) {
            _this.reviews = data;

            _this.applicationRef.tick();

            var total = 0;

            var _iterator = _createForOfIteratorHelper(_this.reviews),
                _step;

            try {
              for (_iterator.s(); !(_step = _iterator.n()).done;) {
                var value = _step.value;
                total = total + value.rating;
              }
            } catch (err) {
              _iterator.e(err);
            } finally {
              _iterator.f();
            }

            _this.average = total / _this.reviews.length;
            if (_this.reviews.length == 0) _this.average = 0;

            _this.calculateAll();

            _this.applicationRef.tick();

            _this.loading.hide();
          });
        } //===============================================================================================================================
        // <!-- 2.0 updates -->

      }, {
        key: "openReviewsPage",
        value: function openReviewsPage() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var modal;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    if (!(this.shared.customerData.id == null || this.shared.customerData.id == undefined)) {
                      _context.next = 9;
                      break;
                    }

                    _context.next = 3;
                    return this.modalCtrl.create({
                      component: _modals_login_login_page__WEBPACK_IMPORTED_MODULE_7__["LoginPage"],
                      componentProps: {
                        'hideGuestLogin': false
                      }
                    });

                  case 3:
                    modal = _context.sent;
                    _context.next = 6;
                    return modal.present();

                  case 6:
                    return _context.abrupt("return", _context.sent);

                  case 9:
                    this.navCtrl.navigateForward(this.config.currentRoute + "/add-review/" + this.id);

                  case 10:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        } //===============================================================================================================================
        // <!-- 2.0 updates -->

      }, {
        key: "totalRating",
        value: function totalRating() {
          var total = 0;

          var _iterator2 = _createForOfIteratorHelper(this.reviews),
              _step2;

          try {
            for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
              var value = _step2.value;
              total = total + value.rating;
            }
          } catch (err) {
            _iterator2.e(err);
          } finally {
            _iterator2.f();
          }

          var result = total;
          if (total == 0) result = 0;
          return result;
        }
      }, {
        key: "calculateAll",
        value: function calculateAll() {
          var r1 = 0,
              r2 = 0,
              r3 = 0,
              r4 = 0,
              r5 = 0;
          var total = this.reviews.length;

          var _iterator3 = _createForOfIteratorHelper(this.reviews),
              _step3;

          try {
            for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
              var value = _step3.value;
              if (value.rating == 1) r1++;
              if (value.rating == 2) r2++;
              if (value.rating == 3) r3++;
              if (value.rating == 4) r4++;
              if (value.rating == 5) r5++;
            }
          } catch (err) {
            _iterator3.e(err);
          } finally {
            _iterator3.f();
          }

          this.r1 = 100 / total * r1;
          if (r1 == 0) this.r1 = 0;
          this.r2 = 100 / total * r2;
          if (r2 == 0) this.r2 = 0;
          this.r3 = 100 / total * r3;
          if (r3 == 0) this.r3 = 0;
          this.r4 = 100 / total * r4;
          if (r4 == 0) this.r4 = 0;
          this.r5 = 100 / total * r5;
          if (r5 == 0) this.r5 = 0;
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return ReviewsPage;
    }();

    ReviewsPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ApplicationRef"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"]
      }];
    };

    ReviewsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-reviews',
      encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./reviews.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/reviews/reviews.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./reviews.page.scss */
      "./src/app/reviews/reviews.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ApplicationRef"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_5__["SharedDataService"]])], ReviewsPage);
    /***/
  }
}]);
//# sourceMappingURL=reviews-reviews-module-es5.js.map