function _createForOfIteratorHelper(o) { if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (o = _unsupportedIterableToArray(o))) { var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var it, normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(n); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cart-cart-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/cart/cart.page.html":
  /*!***************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cart/cart.page.html ***!
    \***************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppCartCartPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button icon=\"arrow-back\"></ion-back-button>\n    </ion-buttons>\n    <ion-title> {{'My Cart'| translate }} </ion-title>\n    <ion-buttons slot=\"end\">\n      <ion-button fill=\"clear\">\n        <ion-icon name=\"basket\"></ion-icon>\n        <ion-badge>{{shared.cartTotalItems()}}</ion-badge>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <ion-grid class=\"ion-no-padding grid-empty\" *ngIf=\"products.length==0 && loadingServerData\">\n    <ion-row class=\"ion-no-padding\">\n      <ion-col size=\"12\" class=\"ion-no-padding\">\n        <ion-icon name=\"basket\"></ion-icon>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"ion-no-padding\">\n      <ion-col size=\"12\" class=\"ion-no-padding\">\n        <h4>{{'Your cart is empty'|translate}}</h4>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"ion-no-padding\">\n      <ion-col size=\"12\" class=\"ion-no-padding\">\n        <h5>{{'continue shopping'|translate}}</h5>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"ion-no-padding\">\n      <ion-col size=\"12\" class=\"ion-no-padding\">\n        <ion-button color=\"secondary\" (click)=\"openProductsPage()\">{{'Explore'|translate}}</ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-card class=\"ion-padding\" *ngFor=\"let product of products\">\n    <ion-card-header class=\"ion-no-padding\">\n      <ion-card-subtitle>\n        <h3> {{product.name}} </h3>\n      </ion-card-subtitle>\n    </ion-card-header>\n    <ion-card-content>\n      <ion-grid class=\"ion-no-padding\">\n        <ion-row>\n          <ion-col size=\"3\">\n            <ion-img class=\"product-image\" src=\"{{product.image}}\"></ion-img>\n          </ion-col>\n          <ion-col size=\"9\">\n            <ion-row>\n              <ion-col class=\"ion-text-left\">\n                {{'Price' |translate}}&nbsp;:\n              </ion-col>\n              <ion-col class=\"ion-text-right\">\n                {{product.price | curency}}\n              </ion-col>\n            </ion-row>\n            <ion-row *ngFor=\"let att of product.meta_data\">\n              <ion-col class=\"ion-text-left\">\n                {{att.key}}&nbsp;:\n              </ion-col>\n              <ion-col class=\"ion-text-right\">\n                {{att.value}}\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col class=\"ion-text-left\">\n                <span style=\"margin: auto;\">{{'Quantity' |translate}}</span>&nbsp;:\n              </ion-col>\n              <ion-col class=\"ion-text-right ion-no-padding\">\n                <ion-row class=\"ion-float-right\">\n                  <ion-icon name=\"remove-circle\" (click)=\"qunatityMinus(product)\" *ngIf=\"!product.sold_individually\">\n                  </ion-icon>\n                  <span class=\"quantity\">{{product.quantity}}</span>\n                  <ion-icon name=\"add-circle\" (click)=\"qunatityPlus(product);\" *ngIf=\"!product.sold_individually\">\n                  </ion-icon>\n                </ion-row>\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col class=\"ion-text-left\">\n                <strong> {{'Sub Total' |translate}}</strong>&nbsp;:\n              </ion-col>\n              <ion-col class=\"ion-text-right\">\n                <strong> {{product.subtotal | curency}}</strong>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n\n        <ion-row>\n          <ion-button color=\"secondary\" (click)=\"getSingleProductDetail(product.product_id)\">{{'View' | translate}}\n          </ion-button>\n          <ion-button fill=\"clear\" color=\"danger\" (click)=\"removeCart(product.cart_id);\">{{'Remove' | translate}}\n          </ion-button>\n        </ion-row>\n      </ion-grid>\n    </ion-card-content>\n  </ion-card>\n  <div *ngIf=\"products.length!=0\">\n    <ion-card class=\"card-product\" *ngFor=\"let coupon of shared.couponArray\">\n      <ion-card-content>\n        <ion-row>\n          <ion-col size=\"6\">\n            {{'Coupon Code'|translate}}\n          </ion-col>\n          <ion-col size=\"6\" class=\"ion-text-right\">\n            {{coupon.code}}\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col size=\"6\">\n            {{'Coupon Amount'|translate}}\n          </ion-col>\n          <ion-col size=\"6\" class=\"ion-text-right\">\n            {{coupon.amount| curency}}\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col size=\"12\" *ngIf=\"coupon.discount_type == 'percent'\">\n            {{'A percentage discount for the entire cart'|translate}}\n          </ion-col>\n          <ion-col size=\"12\" *ngIf=\"coupon.discount_type == 'fixed_cart'\">\n            {{'A fixed total discount for the entire cart'|translate}}\n          </ion-col>\n          <ion-col size=\"12\" *ngIf=\"coupon.discount_type == 'fixed_product'\">\n            {{'A fixed total discount for selected products only'|translate}}\n          </ion-col>\n          <ion-col size=\"12\" *ngIf=\"coupon.discount_type == 'percent_product'\">\n            {{'A percentage discount for selected products only'|translate}}\n          </ion-col>\n          <ion-col size=\"12\">\n            <ion-button small color=\"secondary\" (click)=\"deleteCoupon(coupon.code)\">{{'Remove'|translate}}\n            </ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-card-content>\n    </ion-card>\n\n    <ion-card class=\"ion-padding\">\n      <ion-card-header class=\"ion-no-padding\">\n        <ion-card-subtitle>\n          <h3 id=\"copounlist\" *ngIf=\"!config.appInProduction\" (click)=\"couponslist()\">\n            {{'List of coupon codes'|translate}}\n          </h3>\n        </ion-card-subtitle>\n      </ion-card-header>\n      <ion-card-content>\n        <ion-item>\n          <ion-input placeholder=\"{{'coupon code'|translate}}\" [(ngModel)]=\"c\"></ion-input>\n          <ion-button color=\"secondary\" slot=\"end\" [disabled]=\"c==''\" (click)=\"getCoupon(c)\">{{'Apply'|translate}}\n          </ion-button>\n        </ion-item>\n      </ion-card-content>\n    </ion-card>\n\n  </div>\n\n  <ion-card *ngIf=\"products.length!=0\">\n    <ion-card-content style=\"padding: 10px;\">\n      <ion-row>\n        <ion-col size=\"6\">\n          {{'SubTotal'|translate}}\n        </ion-col>\n        <ion-col size=\"6\" class=\"ion-text-right\">\n          {{subtotal| curency}}\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"6\">\n          {{'Discount'|translate}}\n        </ion-col>\n        <ion-col size=\"6\" class=\"ion-text-right\">\n          - {{(subtotal-total)| curency}}\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"6\">\n          {{'Total'|translate}}\n        </ion-col>\n        <ion-col size=\"6\" class=\"ion-text-right\">\n          {{total| curency}}\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n\n<ion-footer *ngIf=\"products.length!=0\">\n  <ion-button expand=\"full\" solid block color=\"secondary\" (click)=\"proceedToCheckOut()\">\n    {{'Proceed'|translate}}\n  </ion-button>\n</ion-footer>";
    /***/
  },

  /***/
  "./src/app/cart/cart.module.ts":
  /*!*************************************!*\
    !*** ./src/app/cart/cart.module.ts ***!
    \*************************************/

  /*! exports provided: CartPageModule */

  /***/
  function srcAppCartCartModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CartPageModule", function () {
      return CartPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _cart_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./cart.page */
    "./src/app/cart/cart.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts"); // For Translation Language


    var routes = [{
      path: '',
      component: _cart_page__WEBPACK_IMPORTED_MODULE_6__["CartPage"]
    }];

    var CartPageModule = function CartPageModule() {
      _classCallCheck(this, CartPageModule);
    };

    CartPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes), src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"]],
      declarations: [_cart_page__WEBPACK_IMPORTED_MODULE_6__["CartPage"]]
    })], CartPageModule);
    /***/
  },

  /***/
  "./src/app/cart/cart.page.scss":
  /*!*************************************!*\
    !*** ./src/app/cart/cart.page.scss ***!
    \*************************************/

  /*! exports provided: default */

  /***/
  function srcAppCartCartPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-content .grid-empty {\n  margin-top: 50%;\n}\nion-content .grid-empty ion-row ion-col {\n  text-align: center;\n}\nion-content .grid-empty ion-row ion-col ion-icon {\n  zoom: 3.9;\n  color: rgba(var(--ion-text-color-rgb), 0.5);\n}\nion-content .grid-empty ion-row ion-col ion-button {\n  --border-radius: 0px;\n}\nion-content .grid-empty ion-row ion-col h4 {\n  font-size: 16px;\n  font-weight: bold;\n  margin-top: 2px;\n  margin-bottom: 6px;\n}\nion-content .grid-empty ion-row ion-col h5 {\n  font-size: 14px;\n  color: rgba(var(--ion-text-color-rgb), 0.5);\n  margin-top: 0;\n}\nion-content ion-card ion-card-header {\n  border-bottom: solid var(--ion-color-light-shade);\n  border-width: 0.2px;\n}\nion-content ion-card ion-card-header ion-card-subtitle h3 {\n  font-size: 15px;\n  font-weight: bold;\n  margin-top: 0;\n}\nion-content ion-card ion-card-content {\n  padding: 0;\n}\nion-content ion-card ion-card-content .product-image {\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n}\nion-content ion-card ion-card-content ion-icon {\n  font-size: 7vw;\n  vertical-align: -webkit-baseline-middle;\n  color: var(--ion-text-color) !important;\n}\nion-content ion-card ion-card-content .quantity {\n  padding-left: 15px;\n  padding-right: 15px;\n  font-weight: 800;\n  font-size: 4vw;\n  margin: auto;\n}\nion-footer .toolbar {\n  padding-left: 10px;\n}\nion-footer ion-button {\n  height: 45px;\n  margin: 0;\n  margin-bottom: 0;\n}\n.zero-padding-horizontal {\n  padding-left: 0px;\n  padding-right: 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9ncmVnZC9EZXNrdG9wL0dyZWdzIFdvcmtzcGFjZS9Xb3JrL1RyYXZlbFNvZnQvZS1zaG9wIC95ZHJhY29yZnUvc3JjL2FwcC9jYXJ0L2NhcnQucGFnZS5zY3NzIiwic3JjL2FwcC9jYXJ0L2NhcnQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsZUFBQTtBQ0FKO0FERU07RUFDRSxrQkFBQTtBQ0FSO0FEQ1E7RUFDRSxTQUFBO0VBQ0EsMkNBQUE7QUNDVjtBRENRO0VBQ0Usb0JBQUE7QUNDVjtBRENRO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FDQ1Y7QURDUTtFQUNFLGVBQUE7RUFDQSwyQ0FBQTtFQUNBLGFBQUE7QUNDVjtBREtJO0VBQ0UsaURBQUE7RUFDQSxtQkFBQTtBQ0hOO0FES1E7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0FDSFY7QURPSTtFQUVFLFVBQUE7QUNOTjtBRGFNO0VBQ0Usa0JBQUE7RUFDQSxNQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0FDWFI7QURhTTtFQUNFLGNBQUE7RUFDQSx1Q0FBQTtFQUNBLHVDQUFBO0FDWFI7QURhTTtFQUNFLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0FDWFI7QURrQkU7RUFDRSxrQkFBQTtBQ2ZKO0FEaUJFO0VBQ0UsWUFBQTtFQUNBLFNBQUE7RUFDQSxnQkFBQTtBQ2ZKO0FEa0JBO0VBQ0UsaUJBQUE7RUFDQSxrQkFBQTtBQ2ZGIiwiZmlsZSI6InNyYy9hcHAvY2FydC9jYXJ0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcclxuICAuZ3JpZC1lbXB0eSB7XHJcbiAgICBtYXJnaW4tdG9wOiA1MCU7XHJcbiAgICBpb24tcm93IHtcclxuICAgICAgaW9uLWNvbCB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGlvbi1pY29uIHtcclxuICAgICAgICAgIHpvb206IDMuOTtcclxuICAgICAgICAgIGNvbG9yOiByZ2JhKHZhcigtLWlvbi10ZXh0LWNvbG9yLXJnYiksIDAuNSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlvbi1idXR0b24ge1xyXG4gICAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGg0IHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgbWFyZ2luLXRvcDogMnB4O1xyXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogNnB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICBoNSB7XHJcbiAgICAgICAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICAgICAgICBjb2xvcjogcmdiYSh2YXIoLS1pb24tdGV4dC1jb2xvci1yZ2IpLCAwLjUpO1xyXG4gICAgICAgICAgbWFyZ2luLXRvcDogMDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgaW9uLWNhcmQge1xyXG4gICAgaW9uLWNhcmQtaGVhZGVyIHtcclxuICAgICAgYm9yZGVyLWJvdHRvbTogc29saWQgdmFyKC0taW9uLWNvbG9yLWxpZ2h0LXNoYWRlKTtcclxuICAgICAgYm9yZGVyLXdpZHRoOiAwLjJweDtcclxuICAgICAgaW9uLWNhcmQtc3VidGl0bGUge1xyXG4gICAgICAgIGgzIHtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgICAgbWFyZ2luLXRvcDogMDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGlvbi1jYXJkLWNvbnRlbnQge1xyXG4gICAgICAvL2ZvbnQtc2l6ZTogNHZ3O1xyXG4gICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICBpb24tcm93IHtcclxuICAgICAgICAvLyBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIC8vIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICAgIH1cclxuICAgICAgaW9uLWNvbCB7XHJcbiAgICAgIH1cclxuICAgICAgLnByb2R1Y3QtaW1hZ2Uge1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICB0b3A6IDA7XHJcbiAgICAgICAgbGVmdDogMDtcclxuICAgICAgICByaWdodDogMDtcclxuICAgICAgICBib3R0b206IDA7XHJcbiAgICAgIH1cclxuICAgICAgaW9uLWljb24ge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogN3Z3O1xyXG4gICAgICAgIHZlcnRpY2FsLWFsaWduOiAtd2Via2l0LWJhc2VsaW5lLW1pZGRsZTtcclxuICAgICAgICBjb2xvcjogdmFyKC0taW9uLXRleHQtY29sb3IpICFpbXBvcnRhbnQ7XHJcbiAgICAgIH1cclxuICAgICAgLnF1YW50aXR5IHtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTVweDtcclxuICAgICAgICBmb250LXdlaWdodDogODAwO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogNHZ3O1xyXG4gICAgICAgIG1hcmdpbjogYXV0bztcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuaW9uLWZvb3RlciB7XHJcbiAgLnRvb2xiYXIge1xyXG4gICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xyXG4gIH1cclxuICBpb24tYnV0dG9uIHtcclxuICAgIGhlaWdodDogNDVweDtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgfVxyXG59XHJcbi56ZXJvLXBhZGRpbmctaG9yaXpvbnRhbCB7XHJcbiAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgcGFkZGluZy1yaWdodDogMHB4O1xyXG59XHJcbiIsImlvbi1jb250ZW50IC5ncmlkLWVtcHR5IHtcbiAgbWFyZ2luLXRvcDogNTAlO1xufVxuaW9uLWNvbnRlbnQgLmdyaWQtZW1wdHkgaW9uLXJvdyBpb24tY29sIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuaW9uLWNvbnRlbnQgLmdyaWQtZW1wdHkgaW9uLXJvdyBpb24tY29sIGlvbi1pY29uIHtcbiAgem9vbTogMy45O1xuICBjb2xvcjogcmdiYSh2YXIoLS1pb24tdGV4dC1jb2xvci1yZ2IpLCAwLjUpO1xufVxuaW9uLWNvbnRlbnQgLmdyaWQtZW1wdHkgaW9uLXJvdyBpb24tY29sIGlvbi1idXR0b24ge1xuICAtLWJvcmRlci1yYWRpdXM6IDBweDtcbn1cbmlvbi1jb250ZW50IC5ncmlkLWVtcHR5IGlvbi1yb3cgaW9uLWNvbCBoNCB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIG1hcmdpbi10b3A6IDJweDtcbiAgbWFyZ2luLWJvdHRvbTogNnB4O1xufVxuaW9uLWNvbnRlbnQgLmdyaWQtZW1wdHkgaW9uLXJvdyBpb24tY29sIGg1IHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogcmdiYSh2YXIoLS1pb24tdGV4dC1jb2xvci1yZ2IpLCAwLjUpO1xuICBtYXJnaW4tdG9wOiAwO1xufVxuaW9uLWNvbnRlbnQgaW9uLWNhcmQgaW9uLWNhcmQtaGVhZGVyIHtcbiAgYm9yZGVyLWJvdHRvbTogc29saWQgdmFyKC0taW9uLWNvbG9yLWxpZ2h0LXNoYWRlKTtcbiAgYm9yZGVyLXdpZHRoOiAwLjJweDtcbn1cbmlvbi1jb250ZW50IGlvbi1jYXJkIGlvbi1jYXJkLWhlYWRlciBpb24tY2FyZC1zdWJ0aXRsZSBoMyB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIG1hcmdpbi10b3A6IDA7XG59XG5pb24tY29udGVudCBpb24tY2FyZCBpb24tY2FyZC1jb250ZW50IHtcbiAgcGFkZGluZzogMDtcbn1cbmlvbi1jb250ZW50IGlvbi1jYXJkIGlvbi1jYXJkLWNvbnRlbnQgLnByb2R1Y3QtaW1hZ2Uge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG4gIGJvdHRvbTogMDtcbn1cbmlvbi1jb250ZW50IGlvbi1jYXJkIGlvbi1jYXJkLWNvbnRlbnQgaW9uLWljb24ge1xuICBmb250LXNpemU6IDd2dztcbiAgdmVydGljYWwtYWxpZ246IC13ZWJraXQtYmFzZWxpbmUtbWlkZGxlO1xuICBjb2xvcjogdmFyKC0taW9uLXRleHQtY29sb3IpICFpbXBvcnRhbnQ7XG59XG5pb24tY29udGVudCBpb24tY2FyZCBpb24tY2FyZC1jb250ZW50IC5xdWFudGl0eSB7XG4gIHBhZGRpbmctbGVmdDogMTVweDtcbiAgcGFkZGluZy1yaWdodDogMTVweDtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgZm9udC1zaXplOiA0dnc7XG4gIG1hcmdpbjogYXV0bztcbn1cblxuaW9uLWZvb3RlciAudG9vbGJhciB7XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbn1cbmlvbi1mb290ZXIgaW9uLWJ1dHRvbiB7XG4gIGhlaWdodDogNDVweDtcbiAgbWFyZ2luOiAwO1xuICBtYXJnaW4tYm90dG9tOiAwO1xufVxuXG4uemVyby1wYWRkaW5nLWhvcml6b250YWwge1xuICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgcGFkZGluZy1yaWdodDogMHB4O1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/cart/cart.page.ts":
  /*!***********************************!*\
    !*** ./src/app/cart/cart.page.ts ***!
    \***********************************/

  /*! exports provided: CartPage */

  /***/
  function srcAppCartCartPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CartPage", function () {
      return CartPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var src_providers_coupon_coupon_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/providers/coupon/coupon.service */
    "./src/providers/coupon/coupon.service.ts");
    /* harmony import */


    var _ionic_storage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @ionic/storage */
    "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
    /* harmony import */


    var _modals_login_login_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../modals/login/login.page */
    "./src/app/modals/login/login.page.ts");

    var CartPage = /*#__PURE__*/function () {
      function CartPage(navCtrl, shared, config, http, loading, storage, modalCtrl, applicationRef, couponProvider, actionSheetCtrl) {
        _classCallCheck(this, CartPage);

        this.navCtrl = navCtrl;
        this.shared = shared;
        this.config = config;
        this.http = http;
        this.loading = loading;
        this.storage = storage;
        this.modalCtrl = modalCtrl;
        this.applicationRef = applicationRef;
        this.couponProvider = couponProvider;
        this.actionSheetCtrl = actionSheetCtrl;
        this.c = '';
        this.couponArray = [];
        this.products = [];
        this.loadingServerData = true; //============================================================================================  

        this.qunatityPlus = function (p) {
          if (p.stock_quantity == p.quantity) this.shared.showAlert("Product Quantity is Limited!");else if (p.stock_quantity == null || p.stock_quantity > p.quantity) {
            p.quantity++;
            p.subtotal = p.subtotal + parseFloat(p.price);
            p.total = p.total + parseFloat(p.price);
            this.updateCart();
          }
        }; //============================================================================================  
        //function decreasing the quantity


        this.qunatityMinus = function (p) {
          if (p.quantity != 1) {
            p.quantity--;
            p.subtotal = parseFloat(p.price) * p.quantity;
            p.total = parseFloat(p.price) * p.quantity;
            this.updateCart();
          }
        }; //============================================================================================   
        //getting getMostLikedProducts from the server


        this.getCoupon = function (code) {
          var _this = this;

          this.loading.show();
          this.config.getWoo("coupons?code=" + code).then(function (data) {
            _this.loading.hide();

            var d = data;
            var coupon = d[0];
            if (d.length == 0) _this.shared.showAlert("Invalid Coupon Code!");else _this.applyCouponCart(coupon);
          }, function (error) {
            _this.loading.hide();

            console.log(error);
          });
        }; //============================================================================================  
        //applying coupon on the cart


        this.applyCouponCart = function (coupon) {
          //checking the coupon is valid or not
          if (this.couponProvider.validateCouponService(coupon, this.products, this.shared.couponArray) == false) {
            return 0;
          } else {
            if (coupon.individual_use == 1) {
              this.products = JSON.parse(JSON.stringify(this.shared.cartProducts));
              this.shared.couponArray = [];
              console.log('individual_use');
            }

            var v = {};
            v.code = coupon.code;
            v.amount = coupon.amount;
            v.product_ids = coupon.product_ids;
            v.excluded_product_ids = coupon.exclude_product_ids;
            v.product_categories = coupon.product_categories;
            v.excluded_product_categories = coupon.excluded_product_categories;
            v.discount = coupon.amount;
            v.individual_use = coupon.individual_use;
            v.free_shipping = coupon.free_shipping;
            v.discount_type = coupon.discount_type; // v.limit_usage_to_x_items = coupon.limit_usage_to_x_items;
            // v.usage_limit = coupon.usage_limit;
            // v.used_by = coupon.used_by ;
            // v.usage_limit_per_user = coupon.usage_limit_per_user ;
            // v.exclude_sale_items = coupon.exclude_sale_items;

            this.shared.couponArray.push(coupon);
          } //applying coupon service


          this.products = this.couponProvider.apply(coupon, this.products);
          this.updateCart();
        }; //============================================================================================  
        //delete Coupon


        this.deleteCoupon = function (code) {
          var _this2 = this;

          this.shared.couponArray.forEach(function (value, index) {
            if (value.code == code) {
              _this2.shared.couponArray.splice(index, 1);

              return true;
            }
          });
          this.updateCart();
          console.log(this.shared.couponArray);
        };
      } //============================================================================================  


      _createClass(CartPage, [{
        key: "totalPrice",
        value: function totalPrice() {
          var price = 0;
          var subPrice = 0;

          var _iterator = _createForOfIteratorHelper(this.products),
              _step;

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var value = _step.value;
              subPrice = subPrice + value.subtotal;
              price = price + value.total;
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }

          this.subtotal = subPrice;
          this.total = price; // console.log(price);
        }
      }, {
        key: "getSingleProductDetail",
        //============================================================================================  
        value: function getSingleProductDetail(id) {
          var _this3 = this;

          this.loading.show();
          this.config.getWoo("products/" + id + "?" + this.config.productsArguments).then(function (data) {
            _this3.loading.hide();

            _this3.shared.singleProductPageData.push(data);

            if (_this3.config.appNavigationTabs) _this3.navCtrl.navigateForward(_this3.config.currentRoute + "/product-detail/" + data.id);else _this3.navCtrl.navigateForward("/product-detail/" + data.id);
          }, function (err) {
            _this3.loading.hide();

            console.log(err);
          });
        } //============================================================================================  

      }, {
        key: "removeCart",
        value: function removeCart(id) {
          var _this4 = this;

          this.products.forEach(function (value, index) {
            if (value.cart_id == id) {
              _this4.products.splice(index, 1);

              console.log("removing" + id);
            }
          });
          this.shared.removeCart(this.products);
          this.updateCart();
        } //============================================================================================  

      }, {
        key: "proceedToCheckOut",
        value: function proceedToCheckOut() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var modal;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    if (!(this.shared.customerData.id == null || this.shared.customerData.id == undefined)) {
                      _context.next = 8;
                      break;
                    }

                    _context.next = 3;
                    return this.modalCtrl.create({
                      component: _modals_login_login_page__WEBPACK_IMPORTED_MODULE_9__["LoginPage"],
                      componentProps: {
                        'hideGuestLogin': true
                      }
                    });

                  case 3:
                    modal = _context.sent;
                    _context.next = 6;
                    return modal.present();

                  case 6:
                    _context.next = 9;
                    break;

                  case 8:
                    // <!-- 2.0 updates -->
                    if (this.config.checkOutPage == 1) this.shared.onePageCheckOut();else {
                      if (this.config.appNavigationTabs) this.navCtrl.navigateForward(this.config.currentRoute + "/shipping-address");else this.navCtrl.navigateForward("/shipping-address");
                    }

                  case 9:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        } //============================================================================================  

      }, {
        key: "openProductsPage",
        value: function openProductsPage() {
          if (this.config.appNavigationTabs) this.navCtrl.navigateForward("tabs/" + this.config.getCurrentHomePage());else this.navCtrl.navigateForward(this.config.getCurrentHomePage());
        } //============================================================================================  

      }, {
        key: "ionViewWillEnter",
        value: function ionViewWillEnter() {
          //if (this.config.admob == 1) this.shared.showAd();
          this.updateCart();
        } //============================================================================================  

      }, {
        key: "updateCart",
        value: function updateCart() {
          var _this5 = this;

          if (this.shared.cartProducts.length != 0) {
            this.loading.show();
            this.loadingServerData = false;
          }

          var count = 0;
          this.shared.cartProducts.forEach(function (value, index) {
            var id = value.product_id;
            if (value.variation_id != undefined) id = value.variation_id;

            _this5.config.getWoo("products/" + id + "?" + _this5.config.productsArguments).then(function (data) {
              count++;
              var p = data;

              if (p.id == undefined) {
                _this5.shared.cartProducts.splice(index, 1);
              } else if (p.status == 'trash') {
                _this5.shared.cartProducts.splice(index, 1);
              } else {
                value.price = p.price;
                value.name = p.name;
                value.stock_quantity = p.stock_quantity;
                value.tax_status = p.tax_status; //value.image = p.images[0].src;

                value.tax_class = p.tax_class;
                value.tax_status = p.tax_status;
                value.on_sale = p.on_sale;
                value.categories = p.categories;

                if (p.stock_quantity == null) {} else if (p.stock_quantity < value.quantity) value.quantity = p.stock_quantity;else if (p.stock_status != 'instock') {
                  _this5.shared.cartProducts.splice(index, 1);
                }

                value.subtotal = parseFloat(value.price) * value.quantity;
                value.total = parseFloat(value.price) * value.quantity;
              }

              _this5.applicationRef.tick();

              if (count == _this5.shared.cartProducts.length) {
                _this5.changingCart();

                _this5.loading.hide();

                _this5.loadingServerData = true;
              }
            });
          });
        } //==========================================================================

      }, {
        key: "changingCart",
        value: function changingCart() {
          var _this6 = this;

          this.products = this.shared.cartProducts;
          this.storage.set('cartProducts', this.shared.cartProducts);
          this.shared.cartTotalItems();
          this.shared.couponArray.forEach(function (value) {
            _this6.products = _this6.couponProvider.apply(value, _this6.shared.cartProducts);
          });
          this.totalPrice();
          this.applicationRef.tick();
        }
      }, {
        key: "couponslist",
        value: function couponslist() {
          return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var _this7 = this;

            var actionSheet;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return this.actionSheetCtrl.create({
                      header: 'Demo Coupons',
                      buttons: [{
                        icon: 'arrow-forward',
                        text: 'Product Fixed (fp). A fixed total discount for selected products only',
                        handler: function handler() {
                          _this7.c = 'fp';
                        }
                      }, {
                        icon: 'arrow-forward',
                        text: 'Cart Fixed (fc). A fixed total discount for the entire cart',
                        handler: function handler() {
                          _this7.c = 'fc';
                        }
                      }, {
                        icon: 'arrow-forward',
                        text: 'Product Percentage (percentage). A percentage discount for selected products only',
                        handler: function handler() {
                          _this7.c = 'percentage';
                        }
                      }, {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: function handler() {}
                      }]
                    });

                  case 2:
                    actionSheet = _context2.sent;
                    _context2.next = 5;
                    return actionSheet.present();

                  case 5:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        }
      }, {
        key: "ionViewDidEnter",
        value: function ionViewDidEnter() {
          this.shared.getBrowserHeaderColorFromAppHeader();
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return CartPage;
    }();

    CartPage.ctorParameters = function () {
      return [{
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"]
      }, {
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"]
      }, {
        type: _ionic_storage__WEBPACK_IMPORTED_MODULE_8__["Storage"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"]
      }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ApplicationRef"]
      }, {
        type: src_providers_coupon_coupon_service__WEBPACK_IMPORTED_MODULE_7__["CouponService"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ActionSheetController"]
      }];
    };

    CartPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-cart',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./cart.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/cart/cart.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./cart.page.scss */
      "./src/app/cart/cart.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_3__["SharedDataService"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"], _ionic_storage__WEBPACK_IMPORTED_MODULE_8__["Storage"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"], _angular_core__WEBPACK_IMPORTED_MODULE_1__["ApplicationRef"], src_providers_coupon_coupon_service__WEBPACK_IMPORTED_MODULE_7__["CouponService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ActionSheetController"]])], CartPage);
    /***/
  }
}]);
//# sourceMappingURL=cart-cart-module-es5.js.map