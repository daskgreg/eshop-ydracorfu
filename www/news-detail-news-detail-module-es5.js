function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["news-detail-news-detail-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/news-detail/news-detail.page.html":
  /*!*****************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/news-detail/news-detail.page.html ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppNewsDetailNewsDetailPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button icon=\"arrow-back\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>\n        {{post.title.rendered}}\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-img src=\"{{post.image}}\" *ngIf=\"post.image\"></ion-img>\n\n  <h2>{{post.title.rendered}}\n    <br>\n    <small>\n      <ion-icon name=\"clock\"></ion-icon>{{post.date}}\n    </small>\n  </h2>\n  <p [innerHTML]=\"post.content.rendered\"></p>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/news-detail/news-detail.module.ts":
  /*!***************************************************!*\
    !*** ./src/app/news-detail/news-detail.module.ts ***!
    \***************************************************/

  /*! exports provided: NewsDetailPageModule */

  /***/
  function srcAppNewsDetailNewsDetailModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NewsDetailPageModule", function () {
      return NewsDetailPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _news_detail_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./news-detail.page */
    "./src/app/news-detail/news-detail.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _news_detail_page__WEBPACK_IMPORTED_MODULE_6__["NewsDetailPage"]
    }];

    var NewsDetailPageModule = function NewsDetailPageModule() {
      _classCallCheck(this, NewsDetailPageModule);
    };

    NewsDetailPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_news_detail_page__WEBPACK_IMPORTED_MODULE_6__["NewsDetailPage"]]
    })], NewsDetailPageModule);
    /***/
  },

  /***/
  "./src/app/news-detail/news-detail.page.scss":
  /*!***************************************************!*\
    !*** ./src/app/news-detail/news-detail.page.scss ***!
    \***************************************************/

  /*! exports provided: default */

  /***/
  function srcAppNewsDetailNewsDetailPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-content ion-img {\n  margin-right: auto;\n  margin-left: auto;\n}\nion-content h2 {\n  padding-left: 8px;\n  font-size: 20px;\n  font-weight: bold;\n}\nion-content h2 small {\n  display: flex;\n  align-items: center;\n  font-size: 16px;\n  color: #747474;\n  font-weight: normal;\n  margin-top: 5px;\n}\nion-content h2 small .icon {\n  font-size: 18px;\n  margin-right: 5px;\n}\nion-content p {\n  margin-top: auto;\n  padding-left: 8px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9ncmVnZC9EZXNrdG9wL0dyZWdzIFdvcmtzcGFjZS9Xb3JrL1RyYXZlbFNvZnQvZS1zaG9wIC95ZHJhY29yZnUvc3JjL2FwcC9uZXdzLWRldGFpbC9uZXdzLWRldGFpbC5wYWdlLnNjc3MiLCJzcmMvYXBwL25ld3MtZGV0YWlsL25ld3MtZGV0YWlsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFJSTtFQUNJLGtCQUFBO0VBQ0EsaUJBQUE7QUNIUjtBREtJO0VBQ0ksaUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNIUjtBRElRO0VBQ0ksYUFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUNGWjtBREdZO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0FDRGhCO0FES0k7RUFDSSxnQkFBQTtFQUNBLGlCQUFBO0FDSFIiLCJmaWxlIjoic3JjL2FwcC9uZXdzLWRldGFpbC9uZXdzLWRldGFpbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcblxuaW9uLWNvbnRlbnR7XG5cbiAgICBpb24taW1ne1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgIH1cbiAgICBoMntcbiAgICAgICAgcGFkZGluZy1sZWZ0OiA4cHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgIHNtYWxse1xuICAgICAgICAgICAgZGlzcGxheTpmbGV4O1xuICAgICAgICAgICAgYWxpZ24taXRlbXM6Y2VudGVyO1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICAgICAgY29sb3I6ICM3NDc0NzQ7XG4gICAgICAgICAgICBmb250LXdlaWdodDpub3JtYWw7XG4gICAgICAgICAgICBtYXJnaW4tdG9wOjVweDtcbiAgICAgICAgICAgIC5pY29ue1xuICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgICAgICAgICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbiAgICBwe1xuICAgICAgICBtYXJnaW4tdG9wOiBhdXRvO1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDhweDtcbiAgICB9XG59IiwiaW9uLWNvbnRlbnQgaW9uLWltZyB7XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XG59XG5pb24tY29udGVudCBoMiB7XG4gIHBhZGRpbmctbGVmdDogOHB4O1xuICBmb250LXNpemU6IDIwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuaW9uLWNvbnRlbnQgaDIgc21hbGwge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGNvbG9yOiAjNzQ3NDc0O1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICBtYXJnaW4tdG9wOiA1cHg7XG59XG5pb24tY29udGVudCBoMiBzbWFsbCAuaWNvbiB7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5pb24tY29udGVudCBwIHtcbiAgbWFyZ2luLXRvcDogYXV0bztcbiAgcGFkZGluZy1sZWZ0OiA4cHg7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/news-detail/news-detail.page.ts":
  /*!*************************************************!*\
    !*** ./src/app/news-detail/news-detail.page.ts ***!
    \*************************************************/

  /*! exports provided: NewsDetailPage */

  /***/
  function srcAppNewsDetailNewsDetailPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NewsDetailPage", function () {
      return NewsDetailPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");

    var NewsDetailPage = /*#__PURE__*/function () {
      function NewsDetailPage(shared) {
        _classCallCheck(this, NewsDetailPage);

        this.shared = shared;
      }

      _createClass(NewsDetailPage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.post = this.shared.singlePostData;
        }
      }]);

      return NewsDetailPage;
    }();

    NewsDetailPage.ctorParameters = function () {
      return [{
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_2__["SharedDataService"]
      }];
    };

    NewsDetailPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-news-detail',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./news-detail.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/news-detail/news-detail.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./news-detail.page.scss */
      "./src/app/news-detail/news-detail.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_2__["SharedDataService"]])], NewsDetailPage);
    /***/
  }
}]);
//# sourceMappingURL=news-detail-news-detail-module-es5.js.map