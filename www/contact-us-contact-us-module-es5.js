function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["contact-us-contact-us-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/contact-us/contact-us.page.html":
  /*!***************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/contact-us/contact-us.page.html ***!
    \***************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppContactUsContactUsPageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button icon=\"arrow-back\"></ion-back-button>\n    </ion-buttons>\n    <ion-menu-button slot=\"start\" *ngIf=\"!config.appNavigationTabs\">\n      <ion-icon name=\"menu\"></ion-icon>\n    </ion-menu-button>\n    <ion-title>\n      {{'Contact Us'| translate }}\n    </ion-title>\n    <ion-buttons slot=\"end\" *ngIf=\"!config.appNavigationTabs\">\n      <ion-button fill=\"clear\" routerLink=\"/search\" routerDirection=\"forward\">\n        <ion-icon slot=\"icon-only\" name=\"search\"></ion-icon>\n      </ion-button>\n      <ion-button fill=\"clear\" routerLink=\"/cart\" routerDirection=\"forward\">\n        <ion-icon name=\"basket\"></ion-icon>\n        <ion-badge color=\"secondary\">{{shared.cartTotalItems()}}</ion-badge>\n      </ion-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <div #map id=\"map\"></div>\n  <ion-card class=\"ion-no-padding\">\n    <ion-card-content class=\"ion-no-padding\">\n      <ion-item lines=\"none\">\n        <ion-avatar slot=\"start\">\n          <ion-icon name=\"pin\"></ion-icon>\n        </ion-avatar>\n        <ion-label>\n          <p wrap-text>\n            {{config.address}}\n          </p>\n        </ion-label>\n      </ion-item>\n      <ion-item lines=\"none\">\n        <ion-avatar slot=\"start\">\n          <ion-icon name=\"mail\"></ion-icon>\n        </ion-avatar>\n        <ion-label>\n          <p>\n            {{config.email}}\n          </p>\n        </ion-label>\n      </ion-item>\n      <ion-item lines=\"none\">\n        <ion-avatar slot=\"start\">\n          <ion-icon name=\"call\"></ion-icon>\n        </ion-avatar>\n\n        <ion-label>\n          <p>\n            {{config.phoneNo}}\n          </p>\n        </ion-label>\n      </ion-item>\n\n      <form #contactForm=\"ngForm\" (ngSubmit)=\"submit()\">\n        <ion-item>\n          <ion-input type=\"text\" placeholder=\"{{'Name'|translate}}\" name=\"name\" [(ngModel)]=\"contact.name\" required>\n          </ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-input type=\"email\" placeholder=\"{{'Email'|translate}}\" name=\"email\" [(ngModel)]=\"contact.email\" required>\n          </ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-input type=\"text\" placeholder=\"{{'Your Messsage'|translate}}\" name=\"message\"\n            [(ngModel)]=\"contact.message\" required></ion-input>\n        </ion-item>\n        <ion-col col-12>\n          <label *ngIf=\"errorMessage!=''\">\n            <span>{{errorMessage}}</span>\n          </label>\n        </ion-col>\n        <ion-item lines=\"none\">\n          <ion-button expand=\"full\" type=\"submit\" [disabled]=\"!contactForm.form.valid\">{{'Send'|translate}}</ion-button>\n        </ion-item>\n      </form>\n    </ion-card-content>\n  </ion-card>\n</ion-content>";
    /***/
  },

  /***/
  "./src/app/contact-us/contact-us.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/contact-us/contact-us.module.ts ***!
    \*************************************************/

  /*! exports provided: ContactUsPageModule */

  /***/
  function srcAppContactUsContactUsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContactUsPageModule", function () {
      return ContactUsPageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/fesm2015/ionic-angular.js");
    /* harmony import */


    var _contact_us_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./contact-us.page */
    "./src/app/contact-us/contact-us.page.ts");
    /* harmony import */


    var src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/pipes/pipes.module */
    "./src/pipes/pipes.module.ts");

    var routes = [{
      path: '',
      component: _contact_us_page__WEBPACK_IMPORTED_MODULE_6__["ContactUsPage"]
    }];

    var ContactUsPageModule = function ContactUsPageModule() {
      _classCallCheck(this, ContactUsPageModule);
    };

    ContactUsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"], src_pipes_pipes_module__WEBPACK_IMPORTED_MODULE_7__["PipesModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
      declarations: [_contact_us_page__WEBPACK_IMPORTED_MODULE_6__["ContactUsPage"]]
    })], ContactUsPageModule);
    /***/
  },

  /***/
  "./src/app/contact-us/contact-us.page.scss":
  /*!*************************************************!*\
    !*** ./src/app/contact-us/contact-us.page.scss ***!
    \*************************************************/

  /*! exports provided: default */

  /***/
  function srcAppContactUsContactUsPageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "ion-content ion-card ion-card-content ion-item {\n  --inner-padding-end: 0;\n}\nion-content ion-card ion-card-content ion-item ion-avatar {\n  background: #eee;\n}\nion-content ion-card ion-card-content ion-item ion-avatar ion-icon {\n  width: 100%;\n  margin-top: 8px;\n  color: var(--ion-color-primary);\n  zoom: 1.3;\n}\nion-content ion-card ion-card-content ion-item ion-label p {\n  white-space: normal;\n  font-size: 14px;\n}\nion-content ion-card ion-card-content ion-item:last-child {\n  --inner-padding-end: 10px !important;\n  --padding-start: 10px !important;\n}\nion-content ion-card ion-card-content ion-item:last-child ion-button {\n  --background: #83af8c;\n  width: 100%;\n}\n#map {\n  width: 100%;\n  height: 300px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9ncmVnZC9EZXNrdG9wL0dyZWdzIFdvcmtzcGFjZS9Xb3JrL1RyYXZlbFNvZnQvZS1zaG9wIC95ZHJhY29yZnUvc3JjL2FwcC9jb250YWN0LXVzL2NvbnRhY3QtdXMucGFnZS5zY3NzIiwic3JjL2FwcC9jb250YWN0LXVzL2NvbnRhY3QtdXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUlNO0VBQ0Usc0JBQUE7QUNIUjtBRElRO0VBQ0UsZ0JBQUE7QUNGVjtBREdVO0VBQ0UsV0FBQTtFQUNBLGVBQUE7RUFDQSwrQkFBQTtFQUNBLFNBQUE7QUNEWjtBREtVO0VBQ0UsbUJBQUE7RUFDQSxlQUFBO0FDSFo7QURNUTtFQUNFLG9DQUFBO0VBQ0EsZ0NBQUE7QUNKVjtBREtVO0VBQ0UscUJBQUE7RUFDQSxXQUFBO0FDSFo7QURXQTtFQUNFLFdBQUE7RUFDQSxhQUFBO0FDUkYiLCJmaWxlIjoic3JjL2FwcC9jb250YWN0LXVzL2NvbnRhY3QtdXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG5pb24tY29udGVudCB7XG4gIGlvbi1jYXJkIHtcbiAgICBpb24tY2FyZC1jb250ZW50IHtcbiAgICAgIGlvbi1pdGVtIHtcbiAgICAgICAgLS1pbm5lci1wYWRkaW5nLWVuZDogMDtcbiAgICAgICAgaW9uLWF2YXRhciB7XG4gICAgICAgICAgYmFja2dyb3VuZDogI2VlZTtcbiAgICAgICAgICBpb24taWNvbiB7XG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDhweDtcbiAgICAgICAgICAgIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gICAgICAgICAgICB6b29tOiAxLjM7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGlvbi1sYWJlbCB7XG4gICAgICAgICAgcCB7XG4gICAgICAgICAgICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xuICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAmOmxhc3QtY2hpbGQge1xuICAgICAgICAgIC0taW5uZXItcGFkZGluZy1lbmQ6IDEwcHggIWltcG9ydGFudDtcbiAgICAgICAgICAtLXBhZGRpbmctc3RhcnQ6IDEwcHggIWltcG9ydGFudDtcbiAgICAgICAgICBpb24tYnV0dG9uIHtcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZDogIzgzYWY4YztcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG4jbWFwIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMzAwcHg7XG59XG4iLCJpb24tY29udGVudCBpb24tY2FyZCBpb24tY2FyZC1jb250ZW50IGlvbi1pdGVtIHtcbiAgLS1pbm5lci1wYWRkaW5nLWVuZDogMDtcbn1cbmlvbi1jb250ZW50IGlvbi1jYXJkIGlvbi1jYXJkLWNvbnRlbnQgaW9uLWl0ZW0gaW9uLWF2YXRhciB7XG4gIGJhY2tncm91bmQ6ICNlZWU7XG59XG5pb24tY29udGVudCBpb24tY2FyZCBpb24tY2FyZC1jb250ZW50IGlvbi1pdGVtIGlvbi1hdmF0YXIgaW9uLWljb24ge1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luLXRvcDogOHB4O1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xuICB6b29tOiAxLjM7XG59XG5pb24tY29udGVudCBpb24tY2FyZCBpb24tY2FyZC1jb250ZW50IGlvbi1pdGVtIGlvbi1sYWJlbCBwIHtcbiAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuaW9uLWNvbnRlbnQgaW9uLWNhcmQgaW9uLWNhcmQtY29udGVudCBpb24taXRlbTpsYXN0LWNoaWxkIHtcbiAgLS1pbm5lci1wYWRkaW5nLWVuZDogMTBweCAhaW1wb3J0YW50O1xuICAtLXBhZGRpbmctc3RhcnQ6IDEwcHggIWltcG9ydGFudDtcbn1cbmlvbi1jb250ZW50IGlvbi1jYXJkIGlvbi1jYXJkLWNvbnRlbnQgaW9uLWl0ZW06bGFzdC1jaGlsZCBpb24tYnV0dG9uIHtcbiAgLS1iYWNrZ3JvdW5kOiAjODNhZjhjO1xuICB3aWR0aDogMTAwJTtcbn1cblxuI21hcCB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDMwMHB4O1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/contact-us/contact-us.page.ts":
  /*!***********************************************!*\
    !*** ./src/app/contact-us/contact-us.page.ts ***!
    \***********************************************/

  /*! exports provided: ContactUsPage */

  /***/
  function srcAppContactUsContactUsPageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContactUsPage", function () {
      return ContactUsPage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/providers/shared-data/shared-data.service */
    "./src/providers/shared-data/shared-data.service.ts");
    /* harmony import */


    var src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/providers/config/config.service */
    "./src/providers/config/config.service.ts");
    /* harmony import */


    var src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/providers/loading/loading.service */
    "./src/providers/loading/loading.service.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");

    var ContactUsPage = /*#__PURE__*/function () {
      function ContactUsPage(http, config, loading, shared) {
        _classCallCheck(this, ContactUsPage);

        this.http = http;
        this.config = config;
        this.loading = loading;
        this.shared = shared;
        this.contact = {
          name: '',
          email: '',
          message: ''
        };
        this.errorMessage = '';
      }

      _createClass(ContactUsPage, [{
        key: "ionViewDidLoad",
        value: function ionViewDidLoad() {
          if (this.config.googleMapId != "") this.loadMap();
        } // <!-- 2.0 updates -->

      }, {
        key: "submit",
        value: function submit() {
          var _this = this;

          this.loading.autoHide(2000);
          var data = {};
          data = this.contact;
          this.http.get(this.config.url + '/api/appusers/send_mail/?insecure=cool&email=' + this.contact.email + '&name=' + this.contact.name + '&message=' + this.contact.message).subscribe(function (data) {
            console.log(data);
            _this.contact.name = '';
            _this.contact.email = '';
            _this.contact.message = '';
            _this.errorMessage = data;
          }, function (error) {
            _this.errorMessage = JSON.parse(error._body).error;
            console.log(_this.errorMessage);
          });
        }
      }, {
        key: "loadMap",
        value: function loadMap() {
          var myApiKey = this.config.googleMapId;
          var lat = parseFloat(this.config.latitude);
          var lng = parseFloat(this.config.longitude);
          var content = this.config.address;
          var parentElement = this.mapElement.nativeElement;
          var script = document.createElement('script');

          try {
            script.src = "https://maps.googleapis.com/maps/api/js?key=" + myApiKey;
            script.async = true;
            script.defer = true;

            script.onload = function () {
              var map = new google.maps.Map(parentElement, {
                center: {
                  lat: lat,
                  lng: lng
                },
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
              });
              var marker = new google.maps.Marker({
                map: map,
                animation: google.maps.Animation.DROP,
                position: map.getCenter()
              });
              var infoWindow = new google.maps.InfoWindow({
                content: content
              });
              google.maps.event.addListener(marker, 'click', function () {
                infoWindow.open(map, marker);
              });
            };

            this.mapElement.nativeElement.insertBefore(script, null);
          } catch (error) {}
        }
      }, {
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return ContactUsPage;
    }();

    ContactUsPage.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"]
      }, {
        type: src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"]
      }, {
        type: src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"]
      }, {
        type: src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_2__["SharedDataService"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])('map', {
      "static": false
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"])], ContactUsPage.prototype, "mapElement", void 0);
    ContactUsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-contact-us',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./contact-us.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/contact-us/contact-us.page.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./contact-us.page.scss */
      "./src/app/contact-us/contact-us.page.scss"))["default"]]
    }), tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], src_providers_config_config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"], src_providers_loading_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"], src_providers_shared_data_shared_data_service__WEBPACK_IMPORTED_MODULE_2__["SharedDataService"]])], ContactUsPage);
    /***/
  }
}]);
//# sourceMappingURL=contact-us-contact-us-module-es5.js.map